//#define RUN_TEST

#include "ctp.h"
#include "MdSpiImp.h"
#include "TraderSpiImp.h"
#include "Platform.h"
#include "std.h"
#include "Options.h"
#include "Logger.h"
#include "DateTime.h"
#include "Calendar.h"

extern identity self;
vector<str> tickers;

namespace attrs = boost::log::attributes;

void trader_proc(){	
	auto log = Logger::get();	
	log.add_attribute("ThreadName", attrs::constant<std::string>("TD"));	
	ctplog(log, info) << "thread starting.";
	ctplog(log, debug) << "thread id: " << std::this_thread::get_id();
	const char* path = "flow/trade";	
	TraderApi* api = TraderApi::CreateFtdcTraderApi(path);
	if (!api){
		BOOST_LOG_FUNCTION()
		ctplog(log, error) << "CreateFtdcTraderApi(" << path << ") fail. ";
		return;
	}
	TraderSpi* spi = new TraderSpiImp(api);
	ctplog(log, debug) << "launching traderapi lib.";
	api->RegisterSpi(spi);
	api->RegisterFront(self.trader_frontaddr);
	api->SubscribePrivateTopic(THOST_TERT_RESUME);
	api->SubscribePublicTopic(THOST_TERT_RESUME);
	api->Init();
	bool virgin = true;
	while (!Calendar(Calendar::ALL).market_closing()){
		if (virgin){
			ctplog(log, info) << "marketing ...";
			virgin = false;
		}
		Platform::sleep(1000);
	}
	ctplog(log, info) << "thread exiting...";
	api->RegisterSpi(nullptr);
	api->Release();
	delete spi;
	ctplog(log, info) << "thread exit.";
}

void md_proc(){
	auto log = Logger::get();
	namespace attrs = boost::log::attributes;
	log.add_attribute("ThreadName", attrs::constant<std::string>("MD"));
	ctplog(log, info) << "thread starting.";
	ctplog(log, debug) << "thread id: " << std::this_thread::get_id();
	const str path = "./flow/md";
	MdApi* api = MdApi::CreateFtdcMdApi(path.c_str(), false);
	if (!api){
		BOOST_LOG_FUNCTION()
		ctplog(log, error) << "CreateFtdcMdApi(" << path << ") fail. ";
		return;
	}
	ctplog(log, debug) << "launching mdapi lib.";
	MdSpiImp* spi = new MdSpiImp(api, tickers);
	api->RegisterSpi(spi);
	api->RegisterFront(self.md_frontaddr);
	api->Init();
	int prelen = 0;
	ctplog(log, info) << "registering tickers.";
	do{	
		Platform::sleep(1000);
		int len = tickers.size();
		if (len > prelen){
			int count = 0;
			for (int i = prelen; i < len; ++i){
				char* ptr = const_cast<char*>(tickers[i].c_str());
				const int rc = api->SubscribeMarketData(&ptr, 1);
				if (rc != 0){
					ctplog(log, error) << "subscribe " << ptr << " fail, return " << rc << ", ignored.";
				}
				else{
					++count;
				}
			}
			if (count > 0){
				ctplog(log, info) << count << " tickers added for subscription.";
			}
			prelen = len;
		}		
	} while (!Calendar(Calendar::ALL).market_closing());
	ctplog(log, info) << "market closing.";
	spi->MarketClose();
	ctplog(log, info) << "thread exiting.";
	api->RegisterSpi(nullptr);
	delete spi;
	api->Release();
	ctplog(log, info) << "thread exit.";
}

int market_session(){
	int rc = -1;
	bool virgin = true;

	auto log = Logger::get();
	log.add_attribute("ThreadName", attrs::constant<std::string>("SS"));

	while (!Calendar(Calendar::ALL).market_warmup()){
		if (virgin){
			ctplog(log, info) << "waiting for market open.";
			virgin = false;
		}
		Platform::sleep(1000);		
	}
	ctplog(log, info) << "market open.";

	std::thread trader_thread(trader_proc);
	std::thread md_thread(md_proc);

	ctplog(log, debug) << "wait for trade/md thread to finish.";
	trader_thread.join();
	ctplog(log, debug) << "trader thread exit.";
	md_thread.join();
	ctplog(log, debug) << "md thread exit.";
	ctplog(log, info) << "market closed.";
	return 0;
}


bool test_main(char** argv, int argc);

int main(char** argv, int argc){
#ifdef RUN_TEST
	return test_main(argv, argc);
#endif
	const str path[] = { "log", "data", "./flow", "./flow/trade", "./flow/md"};
	for (auto dir : path){		
		Platform::mkdir(dir);
	}
	Logger logger("log/sun");
	auto log = logger.get();
	log.add_attribute("ThreadName", attrs::constant<std::string>("MN"));
	
	while (true){
		ctplog(log, info) << "session start.";
		market_session();
		ctplog(log, info) << "session end, wait for next session.";
		Platform::sleep(1000);
	}
	ctplog(log, info) << "zdream exit.";
	return 0;
}
