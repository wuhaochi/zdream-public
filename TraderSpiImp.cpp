#include <boost/filesystem.hpp>
#include "TraderSpiImp.h"

void TraderSpiImp::OnFrontConnected(void){
	tradingday = ""; // will be reassigned after user login.
	ctplog(log, info)  << "front connected.";
	ReqUserLoginField xfield;
	strncpy(xfield.BrokerID, self.broker, sizeof(self.broker));
	strncpy(xfield.UserID, self.user, sizeof(self.user));
	strncpy(xfield.Password, self.password, sizeof(self.password));		
	const int rc = api->ReqUserLogin(&xfield, RequestID());
	if (fail(rc)){
		ctplog(log, error) << "user login fail, return: " << rc;
		return;
	}
	ctplog(log, info) << "login success.";
}

void TraderSpiImp::OnFrontDisconnected(int nReason){
	str hint = "";
	switch (nReason)
	{
	case 0x1001: hint = "网络读失败"; break;
	case 0x1002: hint = "网络写失败"; break;
	case 0x2001: hint = "接收心跳超时"; break;
	case 0x2002: hint = "发送心跳失败"; break;
	case 0x2003: hint = "收到错误报文"; break;
	default: hint = "错误的nReason值";
	}
	ctplog(log, warning) << "front disconnected: " << hint;
	tradingday = ""; // will be reassigned after user login.
}

void TraderSpiImp::OnRspUserLogin(RspUserLoginField *pfield,
	                                   RspInfoField *pinfo, 
									            int seqid, 
											   bool bIsLast)
{	
	{
		RspUserLoginField& field = *pfield;
		ctplog(log, info) << "user login info:" << endl
			<< "BrokerID: " << field.BrokerID << endl
			<< "CZCETime: " << field.CZCETime << endl
			<< "DCETime: " << field.DCETime << endl
			<< "FFEXTime: " << field.FFEXTime << endl
			<< "FrontID: " << field.FrontID << endl
			<< "LoginTime: " << field.LoginTime << endl
			<< "MaxOrderRef: " << field.MaxOrderRef << endl
			<< "SessionID: " << field.SessionID << endl
			<< "SHFETime: " << field.SHFETime << endl
			<< "SystemName: " << field.SystemName << endl
			<< "TradingDay: " << field.TradingDay;
		tradingday = field.TradingDay;
	}
	afterapi();
	{
		QrySettlementInfoConfirmField field;
		strcpy(field.BrokerID, self.broker);
		strcpy(field.InvestorID, self.user);
		int rc = api->ReqQrySettlementInfoConfirm(&field, RequestID());
		if (fail(rc)){
			ctplog(log, error) << "query settlement info confirm fail, return: " << rc;
		}
	}
}


void TraderSpiImp::OnRspQrySettlementInfoConfirm(SettlementInfoConfirmField *xfield,
	                                                           RspInfoField *pinfo,  
												                        int seqid,        
												                       bool bIsLast)
{
	if (fail(pinfo)){
		ctplog(log, error) << __FUNCTION__ " fail. " << errmsg(pinfo)<< endl;
		return;
	}
	if (xfield){
		ctplog(log, info)
			<< "结算信息确认正确。"
			<< "Broker: " << xfield->BrokerID
			<< "Investor: " << xfield->InvestorID
			<< "ConfirmDate: " << xfield->ConfirmDate
			<< "ConfirmTime: " << xfield->ConfirmTime;
	}
	int rc;
	QryInstrumentField qtfield;
	qtfield.ExchangeID[0] = 0;
	qtfield.ExchangeInstID[0] = 0;
	qtfield.InstrumentID[0] = 0;
	qtfield.ProductID[0] = 0;
	afterapi();
	ctplog(log, info) << "query all tickers.";
	rc = api->ReqQryInstrument(&qtfield, RequestID());
	if (fail(rc)){
		ctplog(log, error) << "ReqQryInstrument fail, return: " << rc;
	}
	
}


void TraderSpiImp::OnRspQryInstrument(InstrumentField *inst,
	                                     RspInfoField *pRspInfo,
										          int seqid, 
												 bool bIsLast)
{
	if (fail(pRspInfo)){
		ctplog(log, info) << __FUNCTION__ " fail. " << errmsg(pRspInfo);
		return;
	}
	if (!inst){		
		return;
	}

	const str filename = str("data/instrument_") + tradingday + ".csv";
	boost::filesystem::path p(filename);
	boost::system::error_code ec;
	bool write_header = !boost::filesystem::exists(p, ec);
	ofstream fout(filename, ios_base::app);	
	const char sep = ',';
	if (write_header){		
		fout << "TradingDate" << sep
			<< "InstrumentID" << sep
			<< "ExchangeID" << sep
			<< "InstrumentName" << sep
			<< "ExchangeInstID" << sep
			<< "ProductID" << sep
			<< "ProductClass" << sep
			<< "DeliveryYear" << sep
			<< "DeliveryMonth" << sep
			<< "MaxMarketOrderVolume" << sep
			<< "MinMarketOrderVolume" << sep
			<< "MaxLimitOrderVolume" << sep
			<< "MinLimitOrderVolume" << sep
			<< "VolumeMultiple" << sep
			<< "PriceTick" << sep
			<< "CreateDate" << sep
			<< "OpenDate" << sep
			<< "ExpireDate" << sep
			<< "StartDelivDate" << sep
			<< "EndDelivDate" << sep
			<< "InstLifePhase" << sep
			<< "IsTrading" << sep
			<< "PositionType" << sep
			<< "PositionDateType" << sep
			<< "LongMarginRatio" << sep
			<< "ShortMarginRatio" << endl;		
	}
	fout << tradingday << sep << *inst << endl;
	tickers.push_back(inst->InstrumentID);
	if (bIsLast){
		ctplog(log, info) << "finish tickers query." << endl;		
	}
	fout.close();
}


void TraderSpiImp::OnRtnInstrumentStatus(InstrumentStatusField* pInstrumentStatus){	
	static const str filename = str("data/instrument_status_") + tradingday + ".csv";
	const char sep = ',';
	boost::filesystem::path p(filename);
	boost::system::error_code ec;
	bool write_header = !boost::filesystem::exists(p, ec);
	ofstream fout(filename, ios::app);
	
	if (write_header){
		fout << "TradingDate" << sep
			<< "ExchangeID" << sep
			<< "ExchangeInstID" << sep
			<< "SettlementGroupID" << sep
			<< "InstrumentID" << sep
			<< "InstrumentStatus" << sep
			<< "TradingSegmentSN" << sep
			<< "EnterTime" << sep
			<< "EnterReason" << endl;
	}
	fout << tradingday << sep << *pInstrumentStatus << endl;
	fout.close();	
}

void TraderSpiImp::OnRspSettlementInfoConfirm(SettlementInfoConfirmField *xfield,
											RspInfoField *rspinfo, int seqid, bool bIsLast){
	ctplog(log, info) << __FUNCTION__ << ":" << __LINE__;
	if (fail(rspinfo)){
		cerr << "FAIL! response of settlement info confirm." << rspinfo->ErrorMsg << endl;
		return;
	}
}

