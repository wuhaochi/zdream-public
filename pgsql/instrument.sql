-- -*- coding: utf-8 *-*

/* instrument list */
drop table if exists instrument;
create table instrument (
       "date" date,             --交易日
       inst text,                  --合约代码
       exch text,               --交易所代码
       "name" text,              --合约名称
       eiid text,           --合约在交易所的代码
       prod text,              --产品代码
       class char,           --产品类型
                                 --'1' Futures 期货,
                                 --'2' Options 期权,
                                 --'3' Combination 组合
                                 --'4' Spot 即期
                                 --'5' EFP 期转现 Exchage for physical.
       deliyear int,
       delimonth int,
       maxmov int,               --max market order volume
       minmov int,               --min market order volume
       maxlov int,               --max limit order volume
       minlov int,               --min limit order volume
       multiplier int,           --volume multipie
       tick double precision,    --price tick
       "create" date,            --create date
       open date,                --open date
       expire date,              --expire date
       startdeliv date,          --开始交割日期
       enddeliv date,            --结束交割日期
       phase char,               --合约生命周期,
                                 --'0' NotStart 未上市,
                                 --'1' Started 上市，
                                 --'2'Pause停,
                                 --'3'Expired到期
       trading bool,             --交易中
       pt char,                --持仓类型, '1'(PT_Net) 净持仓, '2'(PT_Gross) 综合持仓
       pdt char,               --'1' ftdc_pdt_usehistory, 2 pdt_nousehistory
       lmratio double precision, --long margin ratio
       smratio double precision  --short margin ratio
);
