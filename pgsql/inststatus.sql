-- -*- coding: gbk -*-

drop table if exists inststatus;
create table inststatus(
       "date" date,       --trading day
       inst text,               --InstrumentID
       exch text, --ExchangeID
       eiid text, -- ExchangeInstID
       "group" text, --SettlementGroupID
       status char, --InstrumentStatus
-- ///开盘前
-- #define THOST_FTDC_IS_BeforeTrading '0'
-- ///非交易
-- #define THOST_FTDC_IS_NoTrading '1'
-- ///连续交易
-- #define THOST_FTDC_IS_Continous '2'
-- ///集合竞价报单
-- #define THOST_FTDC_IS_AuctionOrdering '3'
-- ///集合竞价价格平衡
-- #define THOST_FTDC_IS_AuctionBalance '4'
-- ///集合竞价撮合
-- #define THOST_FTDC_IS_AuctionMatch '5'
-- ///收盘
-- #define THOST_FTDC_IS_Closed '6'
       sn int, -- TradingSegmentSN 交易阶段编号
       time time, --EnterTime
       reason char -- EnterReason
       --'1' Automatic自动切换, '2'Manual手动切换，'3'Fuse熔断
);
