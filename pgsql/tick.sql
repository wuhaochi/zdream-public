-- -*- coding: gbk *-*

-- 深度行情
drop table if exists tick;
create table tick
(
        "date" date,       -- 交易日
        inst text,         -- 合约代码
        last float8,       -- 最新价
        volume int,        -- 数量
        turnover float8,   -- 成交金额
        ointerest float8,  -- 持仓量 OpenInterest
        hprice float8,     -- 最高价 HighestPrice
        lprice float8,     -- 最低价LowestPrice
        uptime time,       -- 最后修改时间
        upmilli smallint,  -- 最后修改毫秒
        bidp1 float8,      -- 申买价一
        bidv1 int,         -- 申买量一
        askp1 float8,      -- 申卖价一
        askv1 int,         -- 申卖量一
        -- BidPrice2 float8,          -- 申买价二
        -- BidVolume2 int,            -- 申买量二
        -- AskPrice2 float8,          -- 申卖价二
        -- AskVolume2 int,            -- 申卖量二
        -- BidPrice3 float8,          -- 申买价三
        -- BidVolume3 int,            -- 申买量三
        -- AskPrice3 float8,          -- 申卖价三
        -- AskVolume3 int,            -- 申卖量三
        -- BidPrice4 float8,          -- 申买价四
        -- BidVolume4 int,            -- 申买量四
        -- AskPrice4 float8,          -- 申卖价四
        -- AskVolume4 int,            -- 申卖量四
        -- BidPrice5 float8,          -- 申买价五
        -- BidVolume5 int,            -- 申买量五
        -- AskPrice5 float8,          -- 申卖价五
        -- AskVolume5 int,            -- 申卖量五
        mprice float8     -- 当日均价

);

drop table if exists tick2;
create table tick2
(
        "date" date, -- 交易日
        inst text,         -- 合约代码
        exch text,         -- 交易所代码
        psprice float8,    -- 上次结算价        PreSettlementPrice
        pcprice float8,    -- 昨收盘 PreClosePrice
        pointerest float8, -- 昨持仓量 PreOpenInterest
        oprice float8,     -- 今开盘 Open Price
        sprice float8,     -- 本次结算价 settlement price
        cprice float8,     -- 今收盘 close price
        ulprice float8,    -- 涨停板价 Upper Limit Price
        llprice float8,    -- 跌停板价 LowerLimitPrice
        uptime time,       -- 最后修改时间
        upmilli smallint,  -- 最后修改毫秒
        pdelta float8,     -- 昨虚实度 PreDelta
        cdelta float8,     -- 今虚实度 CurrDelta
        eiid text          -- 合约在交易所的代码 ExchangeInstID
);
