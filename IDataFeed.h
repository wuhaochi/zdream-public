#pragma once

#include "ctp.h"

class IDataFeed
{
public:
	virtual 
	void on_data(DepthMarketDataField *pDepthMarketData);

	virtual void maket_open() = 0;
	virtual void market_close() = 0;

	virtual ~IDataFeed();
};

