#pragma once
#include "std.h"
#include "ctp.h"
#include "Platform.h"
#include "Options.h"
#include "Logger.h"

extern identity self;
extern strings tickers;

class TraderSpiImp : public TraderSpi{
private:
	TraderApi*  api;	
	int seqid;
	Logger::logger_t log;
	str	tradingday;
public:
	TraderSpiImp(TraderApi* api) : api(api), seqid(0){
		log = Logger::get();
		log.add_attribute("ThreadName", attrs::constant<std::string>("TD"));
	}

	virtual ~TraderSpiImp(){
	}

	void afterapi(){
		Platform::sleep(1000);
	}
	virtual void OnFrontConnected();

	void OnFrontDisconnected(int nReason);

	void OnRspUserLogin(RspUserLoginField *xfield, RspInfoField *info, int seqid, bool bIsLast);

	///请求查询结算信息确认响应
	void OnRspQrySettlementInfoConfirm(SettlementInfoConfirmField *xfield, RspInfoField *info, int seqid, bool bIsLast);

	void OnRspSettlementInfoConfirm(SettlementInfoConfirmField *pSettlementInfoConfirm,
		RspInfoField *info, int seqid, bool bIsLast);


	void OnRtnInstrumentStatus(InstrumentStatusField* pInstrumentStatus);


	void OnRspQryInstrument(InstrumentField *inst, RspInfoField *pRspInfo, int seqid, bool bIsLast);

	//void OnRspQryInvestor(InvestorField *pInvestor, RspInfoField *pRspInfo, int seqid, bool bIsLast){
	//	ctplog(log, info) << __FUNCTION__ << endl;
	//	if (fail(pRspInfo)){
	//		ctplog(log, info) << "OnRspQryInstrument response error! " << pRspInfo->ErrorMsg << endl;
	//		return;
	//	}
	//	if (!bIsLast){
	//		ctplog(log, info) << "not is last response." << endl;
	//	}
	//	ctplog(log, info) << pInvestor->InvestorName << endl;
	//}

	//void OnRspQryDepthMarketData(DepthMarketDataField *pDepthMarketData,
	//	RspInfoField *pRspInfo,
	//	int seqid, bool bIsLast){
	//	ctplog(log, info) << __FUNCTION__ << endl;
	//}

	//void OnRspOrderInsert(InputOrderField *pInputOrder,
	//	RspInfoField *pRspInfo,
	//	int seqid, bool bIsLast){
	//	if (fail(pRspInfo)){
	//		ctplog(log, info) << __FUNCTION__ << " response error! " << pRspInfo->ErrorMsg << endl;
	//		return;
	//	}

	//	if (!bIsLast){
	//		ctplog(log, info) << "not is last response." << endl;
	//	}
	//}

	//void OnErrRtnOrderInsert(InputOrderField *pInputOrder,
	//	RspInfoField *pRspInfo){
	//	if (fail(pRspInfo)){
	//		ctplog(log, info) << __FUNCTION__ << " response error! " << pRspInfo->ErrorMsg << endl;
	//		return;
	//	}
	//}

	//void OnRspQryAccountregister(AccountregisterField *f,
	//	RspInfoField *pRspInfo,
	//	int seqid,
	//	bool bIsLast){
	//	ctplog(log, info) << __FUNCTION__ << endl;
	//	if (fail(pRspInfo)){
	//		ctplog(log, info) << __FUNCTION__ << " response error! " << pRspInfo->ErrorMsg << endl;
	//		return;
	//	}
	//	ofstream fout("bank id.csv", ios::trunc);
	//	const char* sep = ", ";
	//	fout << "TradeDay" << sep
	//		<< "BankID" << sep
	//		<< "BankBranchID" << sep
	//		<< "BankAccount" << sep
	//		<< "BrokerID" << sep
	//		<< "BrokerBranchID" << sep
	//		<< "AccountID" << sep
	//		<< "IdCardType" << sep
	//		<< "IdentifiedCardNo" << sep
	//		<< "CustomerName" << sep
	//		<< "CurrencyID" << sep
	//		<< "OpenOrDestroy" << sep
	//		<< "RegDate" << sep
	//		<< "OutDate" << sep
	//		<< "TID" << sep
	//		<< "CustType" << sep
	//		<< "BankAccType" << endl;
	//	fout << f->TradeDay << sep
	//		<< f->BankID << sep
	//		<< f->BankBranchID << sep
	//		<< f->BankAccount << sep
	//		<< f->BrokerID << sep
	//		<< f->BrokerBranchID << sep
	//		<< f->AccountID << sep
	//		<< f->IdCardType << sep
	//		<< f->IdentifiedCardNo << sep
	//		<< f->CustomerName << sep
	//		<< f->CurrencyID << sep
	//		<< f->OpenOrDestroy << sep
	//		<< f->RegDate << sep
	//		<< f->OutDate << sep
	//		<< f->TID << sep
	//		<< f->CustType << sep
	//		<< f->BankAccType << endl;
	//	fout.close();
	//}

	//void OnRspQryBrokerTradingAlgos(BrokerTradingAlgosField *f,
	//	RspInfoField *pRspInfo,
	//	int seqid,
	//	bool bIsLast){
	//	ctplog(log, info) << __FUNCTION__ << endl;
	//	if (fail(pRspInfo)){
	//		ctplog(log, info) << __FUNCTION__ << " response error! " << pRspInfo->ErrorMsg << endl;
	//		return;
	//	}
	//	if (!f){
	//		cerr << "result error" << endl;
	//	}

	//}
	//
	//void query_investor(){
	//	ctplog(log, info) << __FUNCTION__ << endl;
	//	strcpy(pInvestor->BrokerID, self.broker);
	//	strcpy(pInvestor->InvestorID, self.investor);
	//	api->ReqQryInvestor(pInvestor, this->RequestID());

	//}

	//void query_depth_market_data(){
	//	QryDepthMarketDataField field;
	//	strcpy(field.InstrumentID, "WH409");
	//	show("query depth market data");
	//	api->ReqQryDepthMarketData(&field, RequestID());
	//}

	//int req_order_insert(){
	//	ctplog(log, info) << __FUNCTION__ << endl;
	//	InputOrderField f;
	//	memset(&f, 0, sizeof(f));
	//	strcpy(f.BrokerID, self.broker);
	//	strcpy(f.InvestorID, self.investor);
	//	strcpy(f.UserID, self.investor); //?
	//	strcpy(f.InstrumentID, "TA409");
	//	f.CombHedgeFlag[0] = THOST_FTDC_HF_Speculation; /* 1 */
	//	f.ContingentCondition = THOST_FTDC_CC_Immediately;
	//	f.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
	//	f.IsAutoSuspend = false;
	//	f.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
	//	f.TimeCondition = THOST_FTDC_TC_GFD;
	//	f.VolumeCondition = THOST_FTDC_VC_AV;
	//	f.MinVolume = 1;
	//	f.LimitPrice = 6600;
	//	f.VolumeTotalOriginal = 5;
	//	f.Direction = THOST_FTDC_D_Buy;
	//	f.CombOffsetFlag[0] = THOST_FTDC_OF_Open;
	//	sprintf(f.OrderRef, "%d", 1038);
	//	return api->ReqOrderInsert(&f, RequestID());
	//}

	//int req_query_bank_account_money_by_future(){
	//	ReqQueryAccountField f;
	//}

	////查签约银行
	//void req_query_account_register()
	//{
	//	show("ReqQueryAccountRegister");
	//	QryAccountregisterField f;
	//	memset(&f, 0, sizeof(f));
	//	strcpy(f.BrokerID, self.broker);
	//	strcpy(f.AccountID, self.investor);
	//	api->ReqQryAccountregister(&f, RequestID());
	//}

	//int req_qeury_broker_trading_algos(){
	//	QryBrokerTradingAlgosField f;
	//	strcpy(f.BrokerID, self.broker);
	//	strcpy(f.InstrumentID, "TA409");
	//	strcpy(f.ExchangeID, "CZCE");
	//	return api->ReqQryBrokerTradingAlgos(&f, RequestID());
	//}
  
    str errmsg(RspInfoField* pinfo){
		if (!pinfo){
			return "";
		}
		using std::stringstream;
		stringstream os;
		os << "Code: " << pinfo->ErrorID << ", Hint: " << pinfo->ErrorMsg;
		return os.str();
   }

	bool success(RspInfoField* pInfo){
		return (!pInfo || !pInfo->ErrorID);
	}

	bool fail(RspInfoField* pInfo){
		return !success(pInfo);
	}
	
	bool fail(const int rc)const
	{
		return 0 != rc;
	}

	void show(const char * procname){
		//Sleep(1000);
	}


	int RequestID(){
		return seqid++;
	}
};
