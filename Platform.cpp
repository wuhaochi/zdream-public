#include "Platform.h"
#include <cstdlib>
#include <direct.h>


Platform::Platform()
{
}


Platform::~Platform()
{
}


int Platform::mkdir(const char * path)
{
#ifdef WIN32	
	return _mkdir(path);
#else
	return mkdir(path);
#endif
	
}
