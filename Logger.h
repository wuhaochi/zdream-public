#pragma once

#include <fstream>
#include <string>
#include <boost/core/null_deleter.hpp>
#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/attributes/constant.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/utility/manipulators/add_value.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace attrs = logging::attributes;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace expr = boost::log::expressions;

/*
code example:
BOOST_LOG_FUNCTION(); 
Logger logger("logs/tick"); filename will be like tick20141029.log
auto log = logger.logger();
//BOOST_LOG_NAMED_SCOPE("named_scope_logging");
BOOST_LOG_SEV(log, normal) << "Hello from the function named_scope_logging!";

*/


BOOST_LOG_ATTRIBUTE_KEYWORD(thread_name, "ThreadName", std::string);


enum severity_level
{
	debug,
	info,
	note,
	warning,
	error,
	critical
};

class Logger
{
public:
	typedef src::severity_logger_mt < severity_level > logger_t ;
	const char* filename_postfix = "%Y%m%d.log";

public:
	Logger(const char* filename);
	/*filename: flie name without suffix.
	e.g: file name abc.txt then filename = abc*/

	static logger_t get(){
		src::severity_logger_mt< severity_level > lg;
		return std::move(lg);
	}

	virtual ~Logger();
};

#ifdef ctplog
#error ctplog is taken, conflict! 
#endif
#define ctplog(logger, level) BOOST_LOG_SEV(log, level)