#pragma once
#include "std.h"

class Platform
{
public:
	Platform();
	virtual ~Platform();
	
	static int mkdir(const char * path);
	static int mkdir(const str path){ return mkdir(path.c_str()); }

	static void sleep(long millisec){
		std::chrono::milliseconds dura(millisec);
		std::this_thread::sleep_for(dura);
	}
};

