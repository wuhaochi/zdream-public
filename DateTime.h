#pragma once
#include <chrono>
using namespace std::chrono;

class Calendar;

class DateTime
{
	friend class Calendar;

	time_t _date;
	time_t _time;

public:
	DateTime();
	DateTime(int yyyymmdd, int HHMMSS);
	virtual ~DateTime();

	int hhmmss()const;
	int yyyymmdd()const;
	time_t time_to_seconds()const;	
};

