#include "Logger.h"

#include <boost/locale/generator.hpp>

#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/utility/formatting_ostream.hpp>
#include <boost/log/utility/manipulators/to_log.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/support/exception.hpp>
#include <boost/log/support/date_time.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources; 
namespace attrs = logging::attributes;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace expr = boost::log::expressions;

// Attribute value tag type
struct severity_tag;

Logger::Logger(const char* filename)
{	
	auto formatter = expr::stream 
		<< "<" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y%m%d.%H:%M:%S") << ">"		
		//<< "<" << expr::attr<unsigned int>("RecordID") << ">"
		<< "<" << expr::attr< severity_level, severity_tag >("Severity") << ">"
		//<< "<" << expr::attr< boost::posix_time::ptime >("TimeStamp") << ">"
		//<< expr::attr<attrs::named_scope::value_type>("Scope")
		<< "<" << expr::attr<std::string>("ThreadName") << ">"
		//<< ":" << expr::attr<std::string>("Message");
		<< ":" << expr::smessage;

	{ // add console log	
		using Core = boost::shared_ptr < logging::core >;		
		using Backend = sinks::text_ostream_backend;
		using Sink = sinks::synchronous_sink <Backend> ;
		auto clog_stream = boost::shared_ptr< std::ostream >(&std::clog, boost::null_deleter());
		Core core = logging::core::get(); 		
		boost::shared_ptr<Backend> backend = boost::make_shared<Backend>();
		backend->add_stream(clog_stream);
		backend->auto_flush();
		boost::shared_ptr<Sink> sink(new Sink(backend));
		sink->set_formatter(formatter);
		core->add_sink(sink);
	}

	{ // add file log
		using Core = boost::shared_ptr < logging::core >;		
		using Backend = sinks::text_file_backend;
		using Sink = sinks::synchronous_sink < Backend > ;
		Core core = logging::core::get(); 
		std::string fn = std::string(filename) + std::string(filename_postfix);
		const auto ROTATE_FILE_SIZE = 100 * 1024 * 1024;
		const std::string fname = std::string(filename);
		auto slash_at = fname.find_first_of('/');
		std::string dir = ".";
		if (slash_at != std::string::npos){
			dir = fname.substr(0 , slash_at);
		}
		auto backend = boost::make_shared<Backend>(
			keywords::file_name =  fn.c_str(),
			keywords::rotation_size = ROTATE_FILE_SIZE,
			keywords::time_based_rotation = sinks::file::rotation_at_time_point(12, 0, 0)			
			);
		backend->auto_flush();
		boost::shared_ptr<Sink> sink(new Sink(backend));
		backend->set_open_mode(std::ios::app);		
		sink->set_formatter(formatter);
		core->add_sink(sink);		
	}

	logging::core::get()->add_global_attribute("RecordID", attrs::counter< unsigned int >());	
	logging::core::get()->add_global_attribute("TimeStamp", attrs::local_clock());	
	logging::core::get()->add_global_attribute("Scope", attrs::named_scope());
	//logging::core::get()->add_global_attribute("ThreadName", attrs::constant);
}

Logger::~Logger()
{

}


// The operator is used for regular stream formatting
std::ostream& operator<< (std::ostream& strm, severity_level level)
{
	static const char* strings[] =
	{
		"debug"
		"info",
		"note",
		"warning",
		"error",
		"critical"
	};

	if (static_cast< std::size_t >(level) < sizeof(strings) / sizeof(*strings))
		strm << strings[level];
	else
		strm << static_cast< int >(level);

	return strm;
}

struct severity_tag;

// The operator is used when putting the severity level to log
using fmt_ostream = logging::formatting_ostream;
using my_log_iomanip = logging::to_log_manip < severity_level, severity_tag >;

fmt_ostream & operator<<(fmt_ostream & strm, my_log_iomanip const& manip)
{
	static const char* strings[] =
	{
		"DBUG",
		"INFO",
		"NOTE",		
		"WARN",
		"ERRR",
		"CRITICAL"
	};

	severity_level level = manip.get();
	if (static_cast< std::size_t >(level) < sizeof(strings) / sizeof(*strings))
		strm << strings[level];
	else
		strm << static_cast< int >(level);

	return strm;
}
