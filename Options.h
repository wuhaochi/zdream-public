#pragma once
#include <string>
#include <boost/program_options.hpp>

#include "ctp.h"
#include "Logger.h"
namespace po = boost::program_options;

using std::string;

class Options
{
	Logger::logger_t log;
public:
	Options();
	~Options();

	void parse(char** argv, int argc){
		string config_file;
		
		// Declare a group of options that will be 
		// allowed only on command line
		po::options_description generic("Generic options");
		generic.add_options()
			("version,v", "print version string")
			("help", "produce help message")
			("config,c", po::value<string>(&config_file)->default_value("sun.cfg"),
			"name of a file of a configuration.");

		// Declare a group of options that will be 
		// allowed both on command line and in
		// config file
		po::options_description config("Configuration");
		config.add_options()
			("db.host", po::value<string>()->default_value("localhost"),
			"postgres database host, default is localhost on windows or /tmp on linux.")
			("db.port", po::value<short>()->default_value(5432), "db port")
			("db.user", po::value<string>()->default_value("postgres"))
			("db.user", po::value<string>()->default_value("postgres"))
			("db.password", po::value<string>());


			//("db.port", po::value<vector<string> >()->composing(), "include path")
			;

		// Hidden options, will be allowed both on command line and
		// in config file, but will not be shown to the user.
		po::options_description hidden("Hidden options");
		hidden.add_options()
			("test", po::value<bool>(), "run test");

		po::options_description cmdline_options;
		cmdline_options.add(generic).add(config).add(hidden);

		po::options_description config_file_options;
		config_file_options.add(config).add(hidden);

		po::options_description visible("Allowed options");
		visible.add(generic).add(config);

		po::positional_options_description p;
		p.add("test", -1); //? -1?

		po::variables_map vm;
		store(po::command_line_parser(argc, argv).
			options(cmdline_options).positional(p).run(), vm);
		notify(vm);

		ifstream ifs(config_file.c_str());
		if (!ifs)
		{
			ctplog(log, error) << "can not open config file: " << config_file;
			return;
		}
		else
		{
			store(parse_config_file(ifs, config_file_options), vm);
			notify(vm);
		}

		//if (vm.count("help")) {
		//	cout << visible << "\n";
		//	return ;
		//}

		//if (vm.count("version")) {
		//	cout << "Multiple sources example, version 1.0\n";
		//	return ;
		//}

		//if (vm.count("include-path"))
		//{
		//	cout << "Include paths are: "
		//		<< vm["include-path"].as< vector<string> >() << "\n";
		//}

		//if (vm.count("input-file"))
		//{
		//	cout << "Input files are: "
		//		<< vm["input-file"].as< vector<string> >() << "\n";
		//}
	}
public:
	template<typename T>
	T get(const string& option){
		return T();
	}
};


struct identity{
	TThostFtdcUserIDType user;
	TThostFtdcBrokerIDType broker;
	TThostFtdcPasswordType password;
	char md_frontaddr[64];
	char trader_frontaddr[64];
};
