#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <thread>
#include <chrono>
#include <memory>

using std::cout;
using std::cerr;
using std::endl;
using std::ends;
using std::copy;
using std::vector;
using str = std::string;
using std::ostream;
using std::fstream;
using std::ofstream;
using std::ifstream;
using std::ios;
using std::ios_base;
using strings = vector < str > ;
using std::shared_ptr;