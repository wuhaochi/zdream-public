#include "DateTime.h"


DateTime::DateTime()
{
	auto point = system_clock::now();
	auto time = system_clock::to_time_t(point);
	auto m = *localtime(&time);
	_date = (m.tm_year + 1900) * 10000 + (m.tm_mon + 1) * 100 + (m.tm_mday);
	_time = m.tm_hour * 10000 + m.tm_min * 100 + m.tm_sec;
}

DateTime::DateTime(int yyyymmdd, int hhmmss){
	_date = yyyymmdd;
	_time = hhmmss;
}

DateTime::~DateTime()
{
}

int DateTime::hhmmss()const
{
	return _time;
}


int DateTime::yyyymmdd()const
{
	return _date;
}


time_t DateTime::time_to_seconds()const
{
	auto _hhmmss = _time;
	const auto hh = _hhmmss / 10000;
	const auto mm = _hhmmss % 10000 / 100;
	const auto ss = _hhmmss % 100;
	return hh * 3600 + mm * 60 + ss;
}