#include "ctp.h"

ostream& operator << (ostream& out, const InstrumentStatusField& field){
	const char sep = ',';
	out << field.InstrumentID << sep 
		<< field.ExchangeID << sep
		<< field.ExchangeInstID << sep
		<< field.SettlementGroupID << sep		
		<< field.InstrumentStatus << sep
		<< field.TradingSegmentSN << sep
		<< field.EnterTime << sep
		<< field.EnterReason;
	return out;
}

ostream& operator << (ostream& out, const InstrumentField& field){
	const char sep = ',';
	out << field.InstrumentID << sep
		<< field.ExchangeID << sep
		<< field.InstrumentName << sep
		<< field.ExchangeInstID << sep
		<< field.ProductID << sep
		<< field.ProductClass << sep
		<< field.DeliveryYear << sep
		<< field.DeliveryMonth << sep		
		<< field.MaxMarketOrderVolume << sep
		<< field.MinLimitOrderVolume << sep 		
		<< field.MaxLimitOrderVolume << sep
		<< field.MinLimitOrderVolume << sep 
		<< field.VolumeMultiple << sep
		<< field.PriceTick << sep
		<< field.CreateDate << sep
		<< field.OpenDate << sep
		<< field.ExpireDate << sep
		<< field.StartDelivDate << sep
		<< field.EndDelivDate << sep
		<< field.InstLifePhase << sep
		<< field.IsTrading << sep
		<< field.PositionType << sep
		<< field.PositionDateType << sep
		<< field.LongMarginRatio << sep
		<< field.ShortMarginRatio;
	return out;
}

ostream& operator <<(ostream& out, const DepthMarketDataField& field){
    const char comma = ',';
    out /*交易日*/           << field.TradingDay         << comma
        /*合约代码*/           << field.InstrumentID       << comma
        /*交易所代码*/         << field.ExchangeID         << comma
        /*合约在交易所的代码*/ << field.ExchangeInstID     << comma
        /*最新价*/             << field.LastPrice          << comma
        /*上次结算价*/         << field.PreSettlementPrice << comma
        /*昨收盘*/             << field.PreClosePrice      << comma
        /*昨持仓量*/           << field.PreOpenInterest    << comma
        /*今开盘*/             << field.OpenPrice          << comma
        /*最高价*/             << field.HighestPrice       << comma
        /*最低价*/             << field.LowestPrice        << comma
        /*数量*/               << field.Volume             << comma
        /*成交金额*/           << field.Turnover           << comma
        /*持仓量*/             << field.OpenInterest       << comma
        /*今收盘*/             << field.ClosePrice         << comma
        /*本次结算价*/         << field.SettlementPrice    << comma
        /*涨停板价*/           << field.UpperLimitPrice    << comma
        /*跌停板价*/           << field.LowerLimitPrice    << comma
        /*昨虚实度*/           << field.PreDelta           << comma
        /*今虚实度*/           << field.CurrDelta          << comma
        /*最后修改时间*/       << field.UpdateTime         << comma
        /*最后修改毫秒*/       << field.UpdateMillisec     << comma
        /*申买价一*/           << field.BidPrice1          << comma
        /*申买量一*/           << field.BidVolume1         << comma
        /*申卖价一*/           << field.AskPrice1          << comma
        /*申卖量一*/           << field.AskVolume1         << comma
        /*申买价二*/           << field.BidPrice2          << comma
        /*申买量二*/           << field.BidVolume2         << comma
        /*申卖价二*/           << field.AskPrice2          << comma
        /*申卖量二*/           << field.AskVolume2         << comma
        /*申买价三*/           << field.BidPrice3          << comma
        /*申买量三*/           << field.BidVolume3         << comma
        /*申卖价三*/           << field.AskPrice3          << comma
        /*申卖量三*/           << field.AskVolume3         << comma
        /*申买价四*/           << field.BidPrice4          << comma
        /*申买量四*/           << field.BidVolume4         << comma
        /*申卖价四*/           << field.AskPrice4          << comma
        /*申卖量四*/           << field.AskVolume4         << comma
        /*申买价五*/           << field.BidPrice5          << comma
        /*申买量五*/           << field.BidVolume5         << comma
        /*申卖价五*/           << field.AskPrice5          << comma
        /*申卖量五*/           << field.AskVolume5         << comma
        /*当日均价*/           << field.AveragePrice;
    return out;
}
