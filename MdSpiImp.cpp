#include "std.h"
#include <limits>
#include <boost/filesystem.hpp>
#include "MdSpiImp.h"

using std::numeric_limits;

extern identity self;

MdSpiImp::MdSpiImp(MdApi* api, strings& tickers)
{
	this->api = api;
	seqid = 0;
	this->tickers = tickers;
	log = Logger::get();
	log.add_attribute("ThreadName", attrs::constant<std::string>("MD"));
}


MdSpiImp::~MdSpiImp()
{
}

bool MdSpiImp::MarketOpen(){
	// call after user login
	const str day = api->GetTradingDay();
	ctplog(log, info) << "trading at " + day << endl;
	const str tick = str("data/tick_") + day + ".csv";
	const str tick2 = str("data/tick2_") + day + ".csv";
	boost::filesystem::path p(tick);
	boost::system::error_code ec;
	bool write_tick_header = !boost::filesystem::exists(p, ec);
	bool write_tick2_header = !boost::filesystem::exists(p, ec);
	whale = ofstream(tick, ios::app);
	if (!whale){
		ctplog(log, error) << "create init whale " << tick;
		return false;
	}
	whale2 = ofstream(tick2, ios::app);
	if (!whale2){
		ctplog(log, error) << "create init whale2 " << tick2;
		return false;
	}
    const char comma = ',';
	if (write_tick_header){
		whale << "交易日" << comma
			<< "合约代码" << comma
			<< "最新价" << comma
			<< "数量" << comma
			<< "成交金额" << comma
			<< "持仓量" << comma
			<< "最高价" << comma
			<< "最低价" << comma
			<< "最后修改时间" << comma
			<< "最后修改毫秒" << comma
			<< "申买价一" << comma
			<< "申买量一" << comma
			<< "申卖价一" << comma
			<< "申卖量一" << comma
			<< "当日均价" << endl;
	}
	if (write_tick2_header){
		whale2 << "交易日" << comma
			<< "合约代码" << comma
			<< "交易所代码" << comma
			<< "昨结算价" << comma
			<< "昨收盘" << comma
			<< "昨持仓量" << comma
			<< "今开盘" << comma
			<< "本次结算价" << comma
			<< "今收盘" << comma
			<< "涨停板价" << comma
			<< "跌停板价" << comma
			<< "最后修改时间" << comma
			<< "最后修改毫秒" << comma
			<< "昨虚实度" << comma
			<< "今虚实度" << comma
			<< "合约在交易所的代码" << endl;
	}
	return true;
}


void MdSpiImp::OnFrontConnected(){
	ctplog(log, info) << "front connected.";
	ReqUserLoginField xfield;
	strncpy(xfield.BrokerID, self.broker, sizeof(self.broker));
	strncpy(xfield.UserID, self.user, sizeof(self.user));
	strncpy(xfield.Password, self.password, sizeof(self.password));
	ctplog(log, info) << "start login " << xfield.UserID << "@" << xfield.BrokerID << "...";
	const int rc = api->ReqUserLogin(&xfield, RequestID());
	if (fail(rc)){
		ctplog(log, error) << "login fail.";
		return;
	}
}

///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
///@param nReason 错误原因
///        0x1001 网络读失败
///        0x1002 网络写失败
///        0x2001 接收心跳超时
///        0x2002 发送心跳失败
///        0x2003 收到错误报文
void MdSpiImp::OnFrontDisconnected(int nReason){
	str hint = "";
	switch (nReason)
	{
	case 0x1001: hint = "网络读失败"; break;
	case 0x1002: hint = "网络写失败"; break;
	case 0x2001: hint = "接收心跳超时"; break;
	case 0x2002: hint = "发送心跳失败"; break;
	case 0x2003: hint = "收到错误报文"; break;
	default: hint = "错误的nReason值";
	}
	ctplog(log, error)<< "front disconnected: " << hint;
	tradingday = "";
}

///心跳超时警告。当长时间未收到报文时，该方法被调用。
///@param nTimeLapse 距离上次接收报文的时间
void MdSpiImp::OnHeartBeatWarning(int nTimeLapse){
}


///登录请求响应
void MdSpiImp::OnRspUserLogin(RspUserLoginField *xfield,
	                               RspInfoField *xinfo,
								            int nRequestID,
										   bool bIsLast)
{
	tradingday = api->GetTradingDay();
	ctplog(log, info) << "login success." << endl;
	for (auto s : tickers){
		char* ptr = const_cast<char*>(s.c_str());
		const int rc = api->SubscribeMarketData(&ptr, 1);
		if (fail(rc)){
			ctplog(log, error)<< "subscribe ticker " << ptr;
		}
	}
	afterapi();
	if (!MarketOpen()){
		ctplog(log, error) << "init market fail.";
	}
}

///登出请求响应
void MdSpiImp::OnRspUserLogout(UserLogoutField *pUserLogout,
	                              RspInfoField *pRspInfo,
								           int nRequestID,
										  bool bIsLast)
{
	ctplog(log, info) << "response of user request logout.";
}

///错误应答
void MdSpiImp::OnRspError(RspInfoField *pRspInfo, int nRequestID, bool bIsLast) {
	str msg = errmsg(pRspInfo);
	if (msg != ""){
		ctplog(log, error) << "response error: " << msg;
	}
	else{
		ctplog(log, error) << "response error";
	}
}

///订阅行情应答
void MdSpiImp::OnRspSubMarketData(SpecificInstrumentField *pinst,
	RspInfoField *pinfo,
	int nRequestID,
	bool bIsLast)
{
	str msg = "";
	if (pinst){
		msg = str(pinst->InstrumentID);
	}
	bool fail = this->fail(pinfo);
	if (fail){
		msg += ", " + errmsg(pinfo);
	}
	if (fail){
		msg = ": " + msg;
		ctplog(log, info) << __FUNCTION__ << msg;
	}
}

///取消订阅行情应答
void MdSpiImp::OnRspUnSubMarketData(SpecificInstrumentField *pinst,
	                                           RspInfoField *pinfo,
											            int nRequestID,
													   bool bIsLast)
{
	str msg = "";
	if (pinst){
		msg = str(pinst->InstrumentID);
	}
	bool fail = this->fail(pinfo);
	if (fail){
		msg += ", " + errmsg(pinfo);
	}
	if (fail){
		msg = ": " + msg;
		ctplog(log, info) << __FUNCTION__ << msg;
	}

}

///深度行情通知
void MdSpiImp::OnRtnDepthMarketData(DepthMarketDataField *pfield) {
	const char comma = ',';
	DepthMarketDataField& field = *pfield;
	whale /*交易日*/     << tradingday << comma
		/*合约代码*/     << field.InstrumentID << comma
		/*最新价*/       << field.LastPrice << comma
		/*数量*/         << field.Volume << comma
		/*成交金额*/     << field.Turnover << comma
		/*持仓量*/       << field.OpenInterest << comma
		/*最高价*/       << field.HighestPrice << comma
		/*最低价*/       << field.LowestPrice << comma
		/*最后修改时间*/ << field.UpdateTime << comma
		/*最后修改毫秒*/ << field.UpdateMillisec << comma
		/*申买价一*/     << field.BidPrice1 << comma
		/*申买量一*/     << field.BidVolume1 << comma
		/*申卖价一*/     << field.AskPrice1 << comma
		/*申卖量一*/     << field.AskVolume1 << comma
		/*当日均价*/     << field.AveragePrice
                         << endl;

	const auto MAXDBL = numeric_limits<double>::max();
	const auto ZERO = 1e-8;
	if ((field.SettlementPrice != 0) && (field.SettlementPrice != MAXDBL))
	{
		whale2 /*交易日*/ << tradingday << comma
			/*合约代码*/ << field.InstrumentID << comma
            /*交易所代码*/ << field.ExchangeID << comma
			/*昨结算价*/ << field.PreSettlementPrice << comma
			/*昨收盘*/ << field.PreClosePrice << comma
			/*昨持仓量*/ << field.PreOpenInterest << comma
			/*今开盘*/ << field.OpenPrice << comma
			/*本次结算价*/ << field.SettlementPrice << comma
			/*今收盘*/ << field.ClosePrice << comma
			/*涨停板价*/ << field.UpperLimitPrice << comma
			/*跌停板价*/ << field.LowerLimitPrice << comma
			/*最后修改时间*/ << field.UpdateTime << comma
			/*最后修改毫秒*/ << field.UpdateMillisec << comma
			/*昨虚实度*/ << field.PreDelta << comma
			/*今虚实度*/ << field.CurrDelta << comma
            /*合约在交易所的代码*/ << field.ExchangeInstID
			<< endl;
	}
	return;
}
